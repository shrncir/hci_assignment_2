// Saves input from input elements to local storage
function saveInputForm(ids) {
    console.log(ids);
    for (i = 0; i < ids.length; i++) {
        var input = document.getElementById(ids[i]);
        var data = input.value;
        console.log('saving: ' + ids[i]+ ', ' + data);
        localStorage.setItem(ids[i], data);
        console.log(localStorage);
    }
}

// Saves input button values to local storage
function saveInputCoach(ids) {
    console.log(ids);
    for (i = 0; i < ids.length; i++) {
        data = $(ids[i]).attr('value');
        console.log('saving: ' + 'coach, ' + data);
        localStorage.setItem('coach', data);
    }
    console.log(localStorage)
}

// Gets data and posts it to the summary table in the html
function loadData(listIDs) {
    console.log('running load Data');
    var inputs = getFromLocalStorage();
    for (i = 0; i < listIDs.length; i++) {
        $('#' + listIDs[i]).text(inputs[i])
    }
}

// Collects data from local storage and returns it as an array
function getFromLocalStorage() {
    var arrayOfInputs = [];
    var keys = ['from', 'to', 'depTime', 'coach', 'whereDD', '', 'addInfo']
    for (i = 0; i < keys.length; i++) {
        var cur = localStorage.getItem(keys[i]);
        arrayOfInputs.push(cur)
    }
    console.log('got here');
    return arrayOfInputs
}

function forgetEverything() {
    localStorage.clear();
    console.log('Forgot everything...')
}

//switches the field that is being shown.
function switchField(nextF) {
    //console.log('test 2');
    $("fieldset").hide(); //hide everything/show this ones container
    $("fieldset[id^='" + nextF + "']").show(); //show the matching fieldset
}

function subPicked(pick) {
    console.log('pick');
    $("#where select").hide();
    $("select[id^='" + pick + "']").show();
    document.getElementById(pick).disabled = false; //show the matching fieldset
}


// --- Help and Info alert functionality ---
// Ideally this method would be refactored and work for all alerts
// function names explain what functions do.
function showAlertHelp() {
    //var whichAlert = $('').attr('class').split(' ').pop();
    $(".help").show();
    $(".leftIcon").attr("onclick", "hideAlertHelp()");
}

function hideAlertHelp() {
    $(".help").hide();
    $(".leftIcon").attr("onclick", "showAlertHelp()");
}

function showAlertInfo() {
    //var whichAlert = $('').attr('class').split(' ').pop();
    $(".info").show();
    $(".rightIcon").attr("onclick", "hideAlertInfo()");
}

function hideAlertInfo() {
    $(".info").hide();
    $(".rightIcon").attr("onclick", "showAlertInfo()");
}

function hideAlerts() {
    hideAlertInfo();
    hideAlertHelp()
}


